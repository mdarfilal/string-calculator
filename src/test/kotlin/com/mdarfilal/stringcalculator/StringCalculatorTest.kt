package com.mdarfilal.stringcalculator

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test

class StringCalculatorTest {

    @Test
    fun shouldReturn0WhenNoInputProvided() {
        assertEquals(0f, StringCalculator.add(""))
    }

    @Test
    fun shouldReturnTheGivenParameterWhenOnlyOneParameterProvided() {
        assertEquals(1f, StringCalculator.add("1"))
    }

    @Test
    fun shouldReturnTheSumWhenTwoParametersProvided() {
        assertEquals(2f, StringCalculator.add("1,1"))
        assertEquals(3f, StringCalculator.add("1,2"))
        assertEquals(3.4f, StringCalculator.add("1.2,2.2"))
    }

    @Test
    fun shouldReturnTheSumOfAllGivenParameters() {
        assertEquals(3f, StringCalculator.add("1,1,1"))
    }

    @Test
    fun shouldTakeIntoAccountNewLineParameter() {
        assertEquals(6f, StringCalculator.add("1\n2,3"))
    }

    @Test
    fun shouldNotifyWhenWrongParameter() {
        assertThrows(NumberFormatException::class.java) {
            StringCalculator.add("175.2,\n35")
        }
    }

    @Test
    fun shouldNotifyWhenMissingParameterAtTheEnd() {
        val exception = assertThrows(NumberFormatException::class.java) {
            StringCalculator.add("1,3,")
        }

        assertEquals("Number expected but EOF found.", exception.message)
    }

    @Test
    fun shouldAllowCustomDelimiter() {
        assertEquals(3f, StringCalculator.add("//;\n1;2"))
        assertEquals(6f, StringCalculator.add("//|\n1|2|3"))
        assertEquals(5f, StringCalculator.add("//sep\n2sep3"))
    }

    @Test
    fun shouldNotifyWhenUnknownDelimiterUsedWithTheCustomOne() {
        assertThrows(NumberFormatException::class.java) {
            StringCalculator.add("//|\n1|2,3")
        }
    }

    @Test
    fun shouldNotifyWhenNegativeNumberProvided() {
        val sum1Exception = assertThrows(NumberFormatException::class.java) {
            StringCalculator.add("-1,2")
        }

        assertEquals("Negative not allowed : [-1]", sum1Exception.message)

        val sum2Exception = assertThrows(NumberFormatException::class.java) {
            StringCalculator.add("2,-4,-5")
        }

        assertEquals("Negative not allowed : [-4, -5]", sum2Exception.message)
    }

}
