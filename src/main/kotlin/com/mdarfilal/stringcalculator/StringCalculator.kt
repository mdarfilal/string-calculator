package com.mdarfilal.stringcalculator

import java.lang.NumberFormatException

class StringCalculator {

    companion object {

        private const val FIRST_DEFAULT_DELIMITER = ","
        private const val SECOND_DEFAULT_DELIMITER = "\n"
        private const val START_OF_CUSTOM_DELIMITER = "//"
        private const val END_OF_CUSTOM_DELIMITER = "\n"

        fun add(input: String): Float {
            if (input.isEmpty()) {
                return 0f
            }
            return getValues(input).sum()
        }

        private fun getValues(input: String): List<Float> {
            val (delimiter, values) = getInputParts(input)

            validateEndOfValues(values)

            val valuesToSum = values.split(*delimiter)

            rejectNegativeValues(valuesToSum)

            return valuesToSum.map(String::toFloat)
        }

        private fun getInputParts(input: String): Pair<Array<String>, String> {
            if (hasCustomDelimiter(input)) {
                val parts = input.split(END_OF_CUSTOM_DELIMITER)
                return Pair(arrayOf(parts[0].substringAfter(START_OF_CUSTOM_DELIMITER)), parts[1])
            }

            return Pair(arrayOf(FIRST_DEFAULT_DELIMITER, SECOND_DEFAULT_DELIMITER), input)
        }

        private fun hasCustomDelimiter(input: String) = input.startsWith(START_OF_CUSTOM_DELIMITER)

        private fun validateEndOfValues(values: String) {
            if (!values.last().isDigit())
                throw NumberFormatException("Number expected but EOF found.")
        }

        private fun rejectNegativeValues(values: List<String>) {
            val negatives = values.filter { it.contains("-") }
            if (negatives.isNotEmpty())
                throw NumberFormatException("Negative not allowed : $negatives")
        }
    }
}
