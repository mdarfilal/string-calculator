package com.mdarfilal.stringcalculator

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class StringCalculatorApplication

fun main(args: Array<String>) {
    runApplication<StringCalculatorApplication>(*args)
}
